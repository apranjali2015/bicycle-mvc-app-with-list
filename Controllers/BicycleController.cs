﻿using Bicycle_MVC.Interface;
using Bicycle_MVC.Models;
using Bicycle_MVC.Repository;
using Microsoft.AspNetCore.Mvc;

namespace Bicycle_MVC.Controllers
{
    public class BicycleController : Controller
    {
        readonly IBicycle _bicyclerepository;

        public BicycleController(IBicycle bicycleRepository)
        {
            _bicyclerepository = bicycleRepository;
        }
        public ActionResult GetAllBicycle()
        {
            List<Bicycle> cycle = _bicyclerepository.GetAllBicycle();
            return View(cycle);
        }
        [HttpGet]
        public ActionResult AddNewBicycle()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddNewBicycle(Bicycle bicycle)
        {
            _bicyclerepository.AddNewBicycle(bicycle);
            return RedirectToAction("GetAllBicycle");
        }

        public ActionResult Delete(int id)
        {
            _bicyclerepository.Delete(id);
            return RedirectToAction("GetAllBicycle");
        }
        public ActionResult GetDetails(int id)
        {
            Bicycle bicycle=_bicyclerepository.GetBicycleById(id);
            return View(bicycle);
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            Bicycle cycle = _bicyclerepository.GetBicycleById(id);
            return View(cycle);
        }
        [HttpPost]
        public ActionResult Edit(int id, Bicycle cycle1)
        {
            bool status = _bicyclerepository.Edit(id, cycle1);
            return RedirectToAction("GetAllBicycle");
        }

        //public IActionResult Index()
        //{
        //    return View();
        //}
    }
}
