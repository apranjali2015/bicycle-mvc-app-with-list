﻿using Bicycle_MVC.Models;

namespace Bicycle_MVC.Interface
{
    public interface IBicycle
    {
        List<Bicycle> GetAllBicycle();
        bool AddNewBicycle(Bicycle bicycle);
        bool Delete(int id);
        Bicycle GetBicycleById(int id);
        bool Edit(int id, Bicycle cycle1);
    }
}
