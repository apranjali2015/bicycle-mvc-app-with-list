﻿namespace Bicycle_MVC.Models
{
    public class Bicycle
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Colour { get; set; }
        public double Price { get; set; }
        
    }
}
