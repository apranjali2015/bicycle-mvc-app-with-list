﻿using Bicycle_MVC.Interface;
using Bicycle_MVC.Models;

namespace Bicycle_MVC.Repository
{
    public class BicycleRepository : IBicycle
    {
        List<Bicycle> bicycles;
        public BicycleRepository()
        {
            bicycles = new List<Bicycle>();
        }

        public bool AddNewBicycle(Bicycle bicycle)
        {
            bicycles.Add(bicycle);
            return true;
        }

        public bool Delete(int id)
        {
            Bicycle cycleexist=GetBicycleById(id);
            if (cycleexist!=null)
            {
                bicycles.Remove(cycleexist);
                return true;
            }
            else 
            { 
                return false; 
            }
            
        }
        public Bicycle GetBicycleById(int id)
        {
            return bicycles.Where(b=>b.Id == id).FirstOrDefault();
        }

        public List<Bicycle> GetAllBicycle()
        {
            return bicycles;
        }

        public bool Edit(int id, Bicycle cycle)
        {
            Bicycle item = GetBicycleById(id);
            //item.Id = user.Id;
            item.Name = cycle.Name;
            item.Colour = cycle.Colour;
            item.Price = cycle.Price;
            return true;
        }
    }
}
